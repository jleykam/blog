+++
Tags = ["life"]
date = "2015-09-27T23:35:21+10:00"
draft = false
title = "Happy"

+++

I have added a new section where I'm trying to write down the things that have
made me happy every week. I do a similar thing at work where I write down
everything that I did during the week, which helps me track where my time is
going and what I've achieved. Hopefully I will be able to track where my happy
is going ☺.

