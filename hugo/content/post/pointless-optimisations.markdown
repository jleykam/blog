+++
Tags = ["programming"]
date = "2014-11-29T21:53:38+11:00"
draft = false
title = "Pointless Optimisations"

+++

Programming, despite being a relatively new field, has its fair share of odd
traditions and practices which are passed about and accepted at face value. Some
of these practices are genuinely useful; things like terminal widths and naming
conventions allow us to communicate and digest information more easily.
Unfortunately not all commonly accepted practices make sense. In this post I
will talk about the existence of so-called 'micro-optimisations' and their
practical effects.

## Definition and example

A 'micro-optimisation' is a very small alteration or practice that can be
applied to a codebase to achieve a slight increase in performance. A well
perpetuated example of such an optimisation is the usage of pre- vs.
post-increment operators (`++i` vs `i++`). The reasoning for such a mandate
seems sensible enough at first glance; a post-increment must store the original
value of the affected variable to be returned after the increment. By always
using pre-increment, we avoid this inefficiency and thus can achieve a very
small increase in speed. In practice, however, how true is this 'wives' tale' of
programming?

Suppose we have a trivial example (in C):

```c
int pre_increment(int a) {
  ++a; return a;
}
int post_increment(int b) {
  b++; return b;
}
```

Compiling this with `gcc -O0 -c increment.c`, then disassembling with `objdump
-d increment.o` gives us the following assembly code:

```
; pre_increment
push   %rbp
mov    %rsp,%rbp
mov    %edi,-0x4(%rbp)
addl   $0x1,-0x4(%rbp)
mov    -0x4(%rbp),%eax
pop    %rbp
retq   
```

```
; post_increment
push   %rbp
mov    %rsp,%rbp
mov    %edi,-0x4(%rbp)
addl   $0x1,-0x4(%rbp)
mov    -0x4(%rbp),%eax
pop    %rbp
retq   
```

Take note here that even when compiling with `-O0` (ie. *every optimisation is
disabled*) there is absolutely *no difference* between the two functions. We can
therefore say that this particular optimisation is at least simple cases
completely, 100% untrue. This process can be applied to many other common
micro optimisations. Simply reduce the 'optimisation' to a small example and
examine the generated assembly code. In the vast majority of cases there will be
no benefit whatsoever.

## Compilers

Disproving every kind of micro-optimisation by analysing the generated assembly
code, whilst effective, would be very tedious. It is much more effective if we
look at the reasons behind *why* micro-optimisations, as a whole, are an
ineffective use of programming time and effort.

The answer to this is that micro-optimisations attempt to exploit low-level
implementation details and behaviours. This is an area that humans are simply
not very good at - and for good reason! Attempting to reason about what the
generated assembly code for a particular statement will be can be extremely
difficult depending on what language you're working in. This is why we have
created very effective tools (compilers) to do this difficult work for us. A
typical compiler (for example, GCC) understands a multitude of different
architectures and the tricks that can be done to exploit them. A compiler can
examine your code and easily work out which areas aren't used, which can be
rearranged to produce better performance and so on. If you honestly believe that
years of research into optimising compilers can be outperformed by some
insignificant tweak in your code ("One weird trick to boost performance!
Compilers hate it!") then that is at best misguided and at worst arrogant. Yes,
one might occasionally be able to use some higher-level knowledge of the program
to rearrange code in a way that a compiler couldn't but generally speaking what
matters most in programming is the *algorithm* you use.

## Profilers

I feel it's bad form to talk about code optimisation in any detail without
mentioning profilers. Profilers are incredibly valuable tools for increasing
the performance of the code because they provide us with a number of essential
metrics, namely:

 - How fast does the program actually run? Do we even need to improve it?
 - Which sections of the program are consuming the most time? Where should we
   focus our efforts?
 - In what specific situations does our program become slow or fast?

Before beginning any kind of performance tuning on a piece of software, whether
it's low-level 'micro-optimisations' or high level algorithm changes, the use of
a profiler is essential as it can be used to guide the entire process. Rely on
hard data to guide your optimisation efforts, not superstition and hunches!
