+++
Tags = ["golang"]
date = "2014-12-22T13:44:28+11:00"
draft = true
title = "Generics Without Generics"

+++

Go 1.4 was released recently. It includes a lot of interesting new features,
including support for creating Android APKs (a feature I intend to explore and
perhaps write about). Unfortunately, as with any time Go is brought up in a
public forum, all sorts of people began coming out of the woodworks to bash Go
for not being everything they hope for in a language (actually the cynic in me
thinks it's for not being Haskell, but...). In particular, Go's lack of generics
is always a hot topic. I feel like many people have a strange view on the
importance of generics, so I will be looking at the widely-taken stance on them
and comparing it to the idiomatic way of representing similar constructs in Go.
I will be comparing Go with C++ for the purposes of this comparison as it is a
lanaguage supporting generics which I am quite familiar with.

Before we leap in, it is important to define not just what is meant by
'generics', but also the kinds of problems which generics are good at solving.
Generics, or Generic Programming (also called parametric polymorphism in
functional programming, or templates in C++-like languages) is a programming
paradigm in which we define algorithms and data structures operating with some
unspecified type which is given at a later point in the program. Typically
static type checking is done when the generic code is instantiated with a
specific type, allowing us to create complex yet robust code which can operate
reliably for any given type.

Generally speaking the problems solved with the use of generics fall into one
of two categories: algorithms which run on types with a certain set of
attributes and data structures (or containers) that organise elements of any
given type.


## Generic algorithms

When we implement a generic algorithm, we typically have some requirements on
what attributes the type specified has. For example, a function what calculates
the absolute value of a type requires that the type can be compared to zero and
that we have some way of negating the value of the type. Such a function can be
trivially implemented in C++:

```c++
template<typename T>
T abs(T num) {
  if (num < 0) {
    return -num;
  }
  return num;
}
```

Proponents of generic programming (specifically the person whose blog this
example is from) try to show how Go is a 'bad language' by naively implementing
the same algorithm in Go:

```go
type Abser interface {
  Negate() Abser
  isNegative() bool
}

func Abs(num Abser) Abser {
  if num.isNegative() {
    return num.Negate()
  }
  return num
}
```

There are good and bad elements to this solution. We should consider the good
first: by using an interface when declaring the type of the function's argument,
we very explicitly state the expectations we have of the given type. Indeed,
we can't actually access any other properties of the given type without having
to do some ugly type coercion. This means that it is very easy to implement a
custom type which can be used in this function.

Unfortunately this solution has a rather glaring flaw: the return type. To
understand why returning an `Abser` isn't something we want to do, we need to
learn a little about the idea behind implicit interfaces.

Basically, the idea of the interface is to specify what we want to be able to do
with the types satisfying it. In the case of `Abser`, we are saying that we want
to be able compare types to zero and negate them (as per the original algorithm
description). Returning an `Abser` from the function is not useful to the caller
as they probably want to be able to do an awful lot more than just these
operations. We should therefore change how the function works so that we don't
make any claims about what the caller can do with the result. We can do this by
modifying the interface:

```go
type Abser interface {
  // Negates the value *in place*
  Negate()
  isNegative() bool
}

func Abs(num Abser) {
  if num.isNegative() {
    num.Negate()
  }
}
```

With this change, we now have a 'generic' `Abs` function that can calculate and
apply the absolute value of any qualifying type. Invoking this function may be
slightly clumsy if we wanted to find the absolute value of a copy of our value,
and I'm sure functional programming fans will bawk at the fact that this
function relies on side-effects, but the point is that this is a solution that
*works*, is *easy to implement* and most importantly *maintains static type
assurances*.

## Containers

Go's interface-based counterpoint to generics tends to fall short in the area of
containers. I've no intention of producing an entire linked list implementation
in C++ and Go just for the sake of a blog post, however I'm sure most people
would agree that the C++ solution would be more elegant. Go's detractors take
this a step further, however, by claiming that Go simply *cannot* implement
containers in a type safe way, a claim that in practical use is completely
false.

In order to implement containers in Go, we need to make liberal use of the empty
interface (`interface{}`). The drawback of this approach is that when it comes
time to return a value from the data structure, we cannot make any static
assurances to the type of the value. This can be solved however with a very
simple wrapper on top of any functions dealing with the data structure's data.
An example illustrates what I mean by this very well. Suppose we have some dumb
data structure:

```go
type Box struct {
  value interface{}
}

func (b *Box) Set(val interface{}) {
  b.value = val
}

func (b Box) Get() interface{} {
  return b.value
}
```

As written, we cannot use it in a type safe way because we lose the static
information about the container's content:

```go
var (
  b Box
  i int
)

b.Set(i)
// At compile time Box.Get() doesn't know that type contained -> we need to
// coerce the type.
i = b.Get().(int)
```

By writing a small wrapper around `Box`, we can solve this issue:

```go
type IntBox Box

func (i *IntBox) IntSet(val int) {
  i.Set(val)
}

func (i IntBox) IntGet() int {
  return i.Get().(int)
}

// ...

var (
  b IntBox
  i int
)

b.IntSet(i)
// All happy!
i = b.IntGet()

```

By sticking rigorously to putting elements in and out of the data structure
through our wrapper, we will for all practical purposes maintain our static type
assurances, even if theoretically we have still lost them. There is a small
amount of boilerplate involved, yes, but I feel that this is an acceptable
trade-off to achieve something that Go's detractors consider "impossible".

## Conclusion

As you can plainly see from the examples given, it is not only possible to solve
the problems typically solved with Generic Programming in Go, but it is possible
to do so whilst maintaining all of the static type safety that generics provide.
