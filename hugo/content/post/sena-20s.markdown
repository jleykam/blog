+++
Tags = ["riding"]
date = "2015-08-08T22:17:45+10:00"
draft = false
title = "Sena 20s: Real world usage"

+++

Daniel and I picked up a pair of [Sena 20ses] a few months ago and have been
using them on every ride together since. I find reviews that talk about real
world usage to be really helpful when choosing products like this, so I thought
I'd write some words on them.

We've been using our Senas since late June/early July. Mostly we've used them
for short trips in urban areas in a variety of conditions.

## Installation

Getting the Senas installed in our helmets was surprisingly easy. It also didn't
require us to use any kind of adhesives so we should be able to take them off
and move them to new helmets without any strife. Basically the way it works is
you use a little allen key to loosen a pair of plates. Then the slot the mount
in the gap between the foam and the shell of your helmet and tighten. Easy!
Threading all the wires through and getting the speakers and microphone
installed is a spot trickier, but the kit you get comes with all sorts of
stickers and pads and velcro so at worst you'll just need to get a little
creative.

{{< img src="sena-install-a.jpg" title="The assembled mount - side" >}}

{{< img src="sena-install-b.jpg" title="The assembled mount - bottom" >}}

My helmet (a Shark Vision R2) is built to support some sort of special Shark
bluetooth system, so I found a couple of round gaps in the cheek pads I could
slot the speakers into. Daniel's helmet is some super cheap LS2 thing he got on
sale and his even had dedicated inset slots for speakers, so I wouldn't expect
to have any trouble with installation on most helmets.

## Pushing buttons (or not)

The Senas include heaps of really neat features. So many that I was originally
really worried about either not having any clue how to work the damn thing, or
worse getting distracted whilst riding. I'm happy to say that neither of those
things is a problem! The 20s includes two different ways of entering commands:
slightly arcane combinations of buttons or an 'Ok Google'-like system.

The first method, pushing buttons, takes a bit of getting used to. There's a
total of three buttons to be pressed plus a turnable knob. Each button can be
either pressed quickly or held in for a bit to activate different features. This
sounds pretty complicated, but in practice it works pretty well. Although I
suppose there's a decent chance that the only reason it seems so simple is that
I only ever use the intercom and radio (can't pair it to my phone whilst I'm on
my Ps!).

The 'Ok Google'-like system (they use 'Hello, Sena') is extremely
straightforward. An absolute joy to use. They recommend a few simple hotwords
you can use for various features, but I've found it's fairly forgiving. For
example for the radio, I've used all of 'FM On', 'FM Radio' and just 'Radio'.
The voice recognition itself seems to be pretty solid. No issues at all even
when the wind is picking up a bit.

## Quality and range

The sound quality of these little things is pretty good. Not exactly crystal
clear, but a hell of a lot better than you might expect. I've never had any
issues hearing Daniel aside from when he's just muttering and not speaking
properly. Worth mentioning is that there must be some kind of dark magic going
on to filter out all the wind noise. I can hear to revving of Daniel's bike even
when we're on a windy road going 90kmph.

The range of the devices hasn't quite been what was advertised thus far. One of
the features the marketing makes a big deal of is the 'up to 2km' range. That's
probably accurate on a nice clear day in the countryside, but in city and
suburban riding we've found that estimate to be highly optimistic. In practice
the range tops out at a few blocks, give or take depending on line of sight. Not
complaining though since even that kind of range is seriously impressive and
more than enough for our purposes.

Even when you do play on the edge of range, the Senas handle it really well. You
get a bit of choppiness, then it beeps to let you know your partner's fallen out
of range. As soon as he comes back, without needing to do anything, it'll
automatically reconnect and you're back to chatting.

## Conclusion

The Sena 20s is a super cool piece of kit. I'm personally really glad we
splashed out on them over the cheaper models like the SMH10 or other brands.
Would definitely recommend it to anyone looking for a solid headset for their
helmet.

[Sena 20ses]: http://www.sena.com/product/20s/

