+++
Tags = ["programming", "android"]
date = "2015-11-14T11:57:08+11:00"
draft = false
title = "What are fragments for?"

+++

[Fragments] in Android are an interesting concept. They essentially provide all
the functionality of Activities within an independent container. Most of the
official documentation and developer guides for fragments talk about using them
for apps that need to support both phone and tablet layouts. Rather than putting
each 'screen' of your app into a new activity, you put it into a fragment. Then
on devices with more space you're able to display multiple screens side by side.
The canonical example for this is a kind of list -> detail view:

{{< img-noscale src="fragments.png" title="List -> detail view with fragments">}}

This example *seems* to be a good fit, but on reading through the developer
guide it starts to get a bit contrived. There's no complex view hierarchies or
animations here, no discussion of how this would interact with your app's data
and no mention of where the business logic of the app fits in. Yes, it's just a
dev guide showing how to get started with fragments but after reading through
I'm no closer to knowing how to actually apply these components in my apps.

Googling about doesn't clear things up much either. In the context of MVP, some
people see fragments as the view, some suggest they serve as controllers or
presenters, whilst some people even suggest that they serve the role of all
three (because mixing your business, view and data logic in one class is the best
idea!). Trying to implement even basic apps using fragments quickly devolves
into awful boilerplate to handle all the cornercases of the Android lifecycle.
For example, here's an extremely basic fragment that contains a TextView,
retaining its contents.

```java

/**
 * A fragment that wraps a TextView. Nothing more. Why is this so big...?
 */
public class ExcitingFragment extends Fragment {
  private static final String KEY_TEXT = "text";

  // Contents of the TextView. Why do we need to keep this separate you ask?
  // Check out onSaveInstanceState.
  private String mText;
  private TextView mTextView;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    // This is the single correct place to put this. If you restore state in
    // onCreateView then your state will disappear if you're in the back stack
    // and the device rotates twice. There's a similar issue if you put this in
    // onActivityCreated, except it applies if you're detached rather than on
    // the back stack.
    if (savedInstanceState != null) {
      mText = savedInstanceState.getString(KEY_TEXT);
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    // Continuing from above, why is savedInstanceState passed in here when it's
    // such an awful trap?
    View root = inflater.inflate(R.layout.some_layout, container, false);
    mTextView = (TextView) root.findViewById(R.id.text_view);

    mTextView.setText(text);
    return root;
  }

  @Override
  public void onDestroyView() {
    // Remember to set all view references to null! Otherwise you'll get
    // difficult to replicate crashes and nasty memory leaks!
    mTextView = null;
    super.onDestroyView();
  }

  public void setText(String text) {
    mText = text;
    // Remember mTextView could be null at any time, since the view might be
    // destroyed.
    if (mTextView != null) {
      mTextView.setText(text);
    }
  }

  @Override
  public void onSaveInstanceState(Bundle bundle) {
    super.onSaveInstanceState(bundle);
    // We can't just save mTextView.getText() because the view might not have
    // been created. Eg. the fragment is on the back stack.
    bundle.putString(KEY_TEXT, mText);
  }
}

```

There's a lot of subtle things going on here that I and I'm sure most other devs
have only learnt through trial and error and squashing an agonising amount of
`NullPointerExceptions`. The dev guide mentions **none** of these things! But
surely I'm doing something wrong. Surely I've just misunderstood and fragments
can be easier to use. Let's check out some of the fragments in the framework.
Something simple like, say, [ListFragment].

Well, shit.

  - Not only does `ListFragment` have to deal with the madness of the views
  sometimes being null, but it's done in the most paranoid way I've ever seen!
  Just about *every method* starts with a call to `ensureList`, which checks the
  view heirarchy still exists.
  - What exactly is `onViewCreated`? None of the docs or dev guides mentioned
  that. Should I be using that instead of setting things up in `onCreateView`?
  - `ListFragment` features the same tedious game of making sure you remember to
  set every view to null when the view hierarchy is destroyed (yes I can see
  *why* it needs to be done. I just think it's a sign of a rubbish API that
  every dev needs to remember to do this or else face memory leaks and random
  crashes).
  - All this and `ListFragment` doesn't even have to deal with retaining some of
  the view's data!

This is clearly not scalable. I speak from experience when I say that as
confusing as this is in the simple case, in any kind of complex app the
fragments often become the single most confusing aspect of the architecture.

So what can we do?

I've been experimenting with some of the ideas presented in 
[Advocating Against Android Fragments] and [Custom Layouts on Android]. TL;DR
instead of using fragments I've been using composite views and separating out
any logic not directly related to views into separate controllers. Thus far this
has worked fantastically well. The code is simple as it doesn't need to deal
being completely detached from an activity and has no weird lifecycles to worry
about. It's also a lot easier to test and, here's the kicker, is *more
reuseable* than equivalent fragment code (remember that reusability is
supposed to be one of the problems fragments solve!).

Anyway, that's my rant.

[Fragments]: http://developer.android.com/guide/components/fragments.html
[ListFragment]: http://grepcode.com/file/repository.grepcode.com/java/ext/com.google.android/android/4.0.1_r1/android/app/ListFragment.java
[Advocating Against Android Fragments]: https://corner.squareup.com/2014/10/advocating-against-android-fragments.html
[Custom Layouts on Android]: http://lucasr.org/2014/05/12/custom-layouts-on-android/
