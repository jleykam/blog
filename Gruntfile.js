/*
 * Gruntfile largely copied from http://tjm.io/grunt-hugo/
 */
module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Takes images placed in /images and resizes them into a thumbnail and
        // larger 'full' size to make them more suitable for the web.
        responsive_images: {
            options: {
                engine: 'im',
                customOut: '-strip',
                newFilesOnly: true,
                sizes: [
                    {name: 'thumb', height: 256, aspectRatio: true},
                    {name: 'full', height: 1024, aspectRatio: true}
                ]
            },
            files: {
                expand: true,
                cwd: "images",
                src: '**.{png,jpg,jpeg}',
                // {%= name %} here is the name of the size. So for example
                // build/images/full/foo.jpg and build/images/thumb/foo.jpg
                custom_dest: 'build/images/{%= name %}/'
            }
        },

        // Compiles styles/main.scss into a single css stylesheet for the entire
        // site.
        sass: {
          dist: {
            options: {
                style: 'compressed',
                sourcemap: 'none'
            },
            files: {
                'build/css/styles.css': 'styles/main.scss'
            }
          }
        },

        // Runs a server for development.
        connect: {
            server: {
                options: {
                    hostname: '127.0.0.1',
                    port: 8080,
                    protocol: 'http',
                    base: 'build',
                    livereload: true
                }
            }
        },

        // Watchs for changed files and reruns tasks as needed.
        watch: {
            options: {
                livereload: true
            },

            // Per-filetype rules
            hugo: {
                files: ['hugo/**'],
                tasks: 'hugo:dev'
            },
            sass: {
                files: ['styles/**'],
                tasks: 'sass:dist'
            },
            images: {
                files: ['images/**'],
                tasks: 'responsive_images'
            },

            // Build everything to start with.
            all: {
                files: ['Gruntfile.js'],
                tasks: 'dev',
                options: {
                    atBegin: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-responsive-images');

    /*
     * Task for running Hugo from within Grunt.
     */
    grunt.registerTask('hugo', function (target) {
        var done = this.async();
        var args = ['--source=hugo', '--destination=../build/'];

        if (target === 'dev') {
            // The 'connect' task serves build/ on this port.
            args.push('--baseUrl=http://127.0.0.1:8080');
            args.push('--buildDrafts=true')
        }

        // Cache-busting is based on an environment variable with the current
        // time. Not the best approach, but it works reasonably well.
        process.env["TIMESTAMP"] = Date.now();

        var hugo = require('child_process').spawn('hugo', args);

        // Ensure we don't swallow Hugo's output as it's often useful.
        var outLogger = function (data) {
            console.log(data.toString().trim());
        };
        hugo.stdout.on('data', outLogger);
        hugo.stderr.on('data', outLogger);

        hugo.on('exit', function (exitCode) {
            done(exitCode == 0);
        });

    });

    /*
     * Task for creating development builds.
     */
    grunt.registerTask('dev', [
        'hugo:dev',
        'sass:dist',
        'responsive_images'
    ]);

    /*
     * Task for creating the public build.
     */
    grunt.registerTask('dist', [
        'hugo:dist',
        'sass:dist',
        'responsive_images'
    ]);

    /*
     * Replacement for 'hugo server'. Starts a server and rebuilds when changes
     * are detected.
     */
    grunt.registerTask('server', [
        'connect',
        'watch'
    ]);

    grunt.registerTask('default', 'server');
};
