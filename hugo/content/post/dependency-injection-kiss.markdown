+++
Tags = ["programming"]
date = 2014-07-19T08:59:37Z
draft = true
title = "Dependency Injection: KISS!"
+++

I've previously written a kinda rambling post about the basics of
dependency injection and why it's useful. Since then I've seen a whole bunch of
posts and libraries and such online about the topic and have been completely
blown away by how complicated people seem to make the concept. I thought it
might be vaguely helpful to strip back a lot of the crazy boilerplate people
have come up with and compare it to the basics.

## A simple definition

Dependency injection is really a super simple concept. You can define it neatly
in a single sentence; "providing an object with instances of objects it needs
rather than having it create them itself". For example:

Suppose we have some class Foo, which requires an instance of Bar to perform
some of its operations:

```c++
class Foo {
  protected:
    Bar *m_bar;
};
```

A question we might ask is "where does the value for `m_bar` come from"? You
might choose to create it during Foo's constructor;

```c++
Foo::Foo() {
  m_bar = new Bar();
}
```

However when following dependency injection we should create the value for
`m_bar` outside of Foo and then pass it either via the constructor or a setter
method:

```c++
Foo::Foo(Bar *b) {
  m_bar = b;
}
```

...and that's it! That's really all there is to a simple definition of
dependency injection. We don't need any complicated libraries or 'injector'
classes or anything of the sort. Yes, these things can be very useful in
more complicated situations, however for the majority of cases this example is
all you really need to know. Keep it simple, stupid!
