+++
Tags = ["golang", "programming"]
date = 2014-06-09T05:13:02Z
draft = false
title = "Pointer vs value receivers"
+++

The difference between pointer and value receivers for struct methods in Go is
subtle, yet very important. A method's receiver type effectively governs:

 - the lifetime of changes made to the receiver,
 - to a small extent, the overhead involved in calling the method,
 - and the struct's fulfillment of interfaces,

## Lifetime of changes

The most obvious effect of the method receiver's type is the 'lifetime' of
changes made by the function. A simple example of this is shown below:

```go
type foo int

func (f foo) addOne() {
  f++
}
```

In this (admittedly trivial) example, the change made to foo in the `addOne`
method will never actually have any effect outside of its scope. This
behaviour may seem strange to people who are unfamiliar with either pointers
or Go's syntax for method receivers, but the explanation is in fact quite
simple. The function declaration syntax above is simply syntactic sugar for
the following:

```go
func addOne(f foo)
```

When the declaration is shown in this way it is easy to see that 'f' in this
method is just a copy of the struct the method operates on. As such, a fix for
our original code would be to use a pointer receiver:

```go
type foo int

func (f *foo) addOne() {
  f++
}
```

## Method call overhead

This aspect of method receivers is possibly the most trivial, however it is
still a very important aspect to consider. Ignoring for a moment any
optimisations the Go compiler may or may not be performing, declaring a function
using a value receiver will result in the entire struct instance being copied.
In small examples this isn't a problem, however if our structure contains a
large amount of data (say, an image) then we definitely want to use a pointer
receiver.

## Satisfying interfaces

The final consideration when choosing the receiver type of a method is the most
confusing by far and is best illustrated by an example:

```go
package main

type foo int
type bar int

type adder interface {
  add()
}

func (f foo) add() {}
func (b *bar) add() {}

func addTwo(a adder) {
  a.add()
  a.add()
}

func main() {
  var (
    f foo
    b bar
  )

  addTwo(f)
  addTwo(b)
  addTwo(&f)
  addTwo(&b)
}
```

This example does not compile, however it can be very tricky (especially for
beginners!) to explain why. I couldn't even tell you off the top of my head
because the rules for satisfying interfaces in the context of method receivers
are rather convoluted.

For the record, the problem with this code is the call `addTwo(b)`.
Hopefully we should be able to deduce both a reason for why we can't make this
call and then from that a general rule for situations such as this.

First let's look at the three valid calls made;

`addTwo(f)`: `addTwo` expects something with a `add()`
method. `foo` has such a method and the type of `f` is foo. There is no mystery
here, every works as we expect.

`addTwo(&f)`: `addTwo` expects something with a `add()`
method. `foo` has such a method, however the type of `&f` is \*foo. So why does
this work? The reasoning here is that if you have a pointer to something then
you can easily dereference that pointer to obtain the value. In others words,
we could just as easily replace this call with `addTwo(*(&f))` and it
would work correctly.

`addTwo(&b)`: As with the first call, `addTwo` expects something
with a `add()` method. `*bar` has such a method and b is of type `*bar`.
No magic here, either.

Now let's look at the call that we can't do:

`addTwo(b)`: The `addTwo` method expects something with a
`add()` method. It is given b, which has type `bar`. `bar` does not have
this method, by `*bar` does. So why doesn't this work? Why can't it just convert
the type as we saw eariler with `&f`? There are a lot reasonable arguments for
and against this behaviour, but at the end of the day the Go specification, for
whatever reason, has deemed that this particular conversion will not be done
implicitly.

## Rule for interface satisfaction

From the above it is possible to derive a simple set of rules for which
functions of a struct instance we can call:

 - If we are given a pointer or value, we can call the corresponding methods,
 - Additionally, if we have a pointer then we can call the value methods.

Which can be inversely expressed as:

 - If we are given a value then we cannot explicity call pointer methods.

