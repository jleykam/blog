+++
Tags = ["life", "riding"]
date = "2015-05-18T21:15:28+10:00"
draft = false
title = "Picnic!"
+++

Daniel and I went for a ride to [Lane Cove National Park] last weekend! We had a
lot of fun on the ride and enjoyed a lovely picnic together. What better a time
to play around with adding a few photos?

Here are our stickered helmets! We got the stickers a few weeks ago. They look
pretty sweet, even if we did kinda fail at applying them nicely. (Oh, note the
nice flowers on the table... I also graduated from university the day the
stickers came!)

{{< img src="helmets.jpg" title="Stickers!" >}}

The ride over there was pretty easy. The main road through the whole park
(Riverside Drive) was also quite lovely. It's slow and full of bends and the
scenery is just so peaceful. I didn't know there were any places like this so
close to Chatswood.

Here's a nice photo of our bikes, and a silly glamour shot against the city.

{{< img src="bikes.jpg" title="Our fun bikes." >}}

{{< img src="skyline.jpg" title="I think this is called a city skyline? Possibly? I'm really not sure." >}}

That's all for now!

[Lane Cove National Park]: https://www.google.com.au/maps/place/Lane+Cove+National+Park/@-33.775543,151.1245328,13z/data=!3m1!4b1!4m2!3m1!1s0x6b12a6163f1d2285:0xf017d68f9f29f10
