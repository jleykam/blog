# blog #

This is the repository in which I keep my blog. You can see it hosted at
[jacqueline.id.au](https://jacqueline.id.au/).


### Depencencies ###

If for whatever reason you want to build this, you'll need the following things:

- [hugo](http://hugo.spf13.com/) (v0.15)
- [grunt](http://gruntjs.com) (v0.1.13)
- [sass](http://sass-lang.com/) (v3.4.20)
- ImageMagick

Why do webby things always have such scary looking version numbers?

### Building ###

First you'll need to install of all the bits and bobs used by grunt:

```
npm install
```

Then just run grunt and it should build everything and start up a server with
livereload.

```
grunt
```
