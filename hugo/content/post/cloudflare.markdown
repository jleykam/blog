+++
Tags = ["webdev"]
date = "2015-09-03T21:39:02+10:00"
draft = false
title = "CloudFlare and analytics"

+++

I'm using CloudFlare now! CloudFlare is a neato CDN-cum-reverse proxy that
offers a bunch of services. Aside from the usual CDN-y things of making
everything load just a little bit faster, they also offer a sort of 'fake' SSL
that looks like the real thing (no real added security, but it means you can
make use of things like SPDY), request query rewriting tools and even their own
kind of cache busting.

I obviously don't need any of these things for a dumb blog nobody will ever read
(also I already rolled my own cache busting with hugo), but it was fun to setup
and I get to feel a little bit more secure in the over-engineeredness of my
site.
