+++
Tags = ["miniatures"]
date = "2015-12-01T18:49:56+11:00"
draft = false
title = "Stormtroopers"

+++

On Sunday Daniel and I had some time to kill in Chatswood. We've been watching a
brilliant series on miniature painting by [Sorastro], so we thought why not take
a look at paints. One thing led to another and we accidentally bought a bunch of
paints, some brushes and a primer. Then we did this:

{{< img src="stormtroopers.jpg" title="Stormtroopers" >}}

They look so cool! And it was pretty easy, too! I tried painting minis once when
I was quite young but didn't have the patience for it, so the results never
looked as nice as this.

[Sorastro]: https://www.youtube.com/watch?v=1ZUsfAtyEM0
