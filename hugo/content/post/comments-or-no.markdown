+++
Tags = ["webdev"]
date = "2015-03-14T20:00:32+11:00"
draft = false
title = "Commenting in Static Sites"

+++

I'm thinking of removing comments from my site (again). Practically speaking it
makes little difference, as nobody actually looks at this site and that's kind
of a prerequisite for getting comments, but I like to use my little site as a
playground for messing about with web stuff and I'm not convinced that there are
any comment platforms out there that fit my requirements.

Some background: this site has had no comments, then disqus, then nothing again
and at the moment I'm using [muut](moot.com). I feel that comments are a
relatively important part of a blog site since they allow readers to interact
with the author, add their own perspectives and perhaps offer corrections.
There's probably also some fancy studies about that say they aid user engagement,
which promotes some other buzzword, blah blah blah. At the same time, dynamic
user content is hard to get right and involves some non-trivial architectural
challenges when used in static sites like this.

The main issues I've had with the commenting system I've tried and looked at
are:

 - Intrusive tracking
 - High setup and maintenance requirements
 - HTML5 compliance

## Tracking

This was my biggest issue with disqus. I understand that when using a service
hosted on someone else's server it is completely reasonable for them to want to
see some kind of value in return. They've got to keep their lights on, after
all. If all they injected into the page were some low-key ads (how effectively
does AdWords work?) then I would be completely happy with that! Unfortunately
disqus has chosen, for whatever reason, to take the shadier route: they record
various analytical data about the users of sites using their comments system.
Whilst I'm vaguely irritated by people constantly jumping on the "X sells your
data!" bandwagon without evidence, I see no other explanation for what they
could be doing with this data.

## Setup Costs

Not monetary ones, either! If I actually got traffic here then I would be more
than happy to throw a bit of cash into a good system.

This is basically a problem with all of the self-hosted comments systems I
looked at. I mean there's basic requirements that any commenting system would
have; it would need to run some kind of server, have a database, that sort of
this. My issue is that all of the solutions I found had a dozen or so pointless
dependencies that require some kind of intricate setup. Most of the time these
dependencies weren't even strictly necessary and were simply included to support
some complex feature or another that I don't care about. I want a solution that
can be setup basically like this:

 1. Install a package
 2. *Maybe* make some simple changes to a config file. Just setting up site
    specific stuff like what its URLs will look like.
 2. Configure nginx/Apache/whatever to proxy certain paths to the service
 3. Add a snippet to your pages wherever you want comments to appear.

Anything more than this (especially manually setting up dependencies; I have a 
package manager for this!) and I it's really not worth the effort.

## HTML5 Compliance

Call me unrealistic, but I think that websites should consist strictly of
well-formed markup. In fact I think it's a shocking state of affairs that most
browsers will take total trash and try to make something useful out of it; it's
a messy practice that ultimately leads to inconsistency and developer headaches.
With that in mind, I do what I can to try to make every part of my site
compliant with the latest standards. Unfortunately the embedded commenting
options I've seen all do some strange things with their markup. It's all
relatively 'safe' stuff that all browsers are going to deal with predictably,
but it's malformed all the same and that annoys me.

## What to do?

I suppose at the end of the day I'll just stick with muut for now. It's simple,
it's not super horrible and it seems to work pretty well. Maybe sometime I'll
make my own self hosted solution that doesn't suffer from all the problems of
the ones I've seen (where's that xkcd...). Maybe I'll just shove my email
address on the bottom of every page and say to yell at me if there's any
problems, who knows?

If anyone does happen to stumble across my drabbles, then do say hello! I'd be
fascinated to know how you got here! :)
