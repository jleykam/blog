+++
Tags = ["programming"]
date = "2016-01-18T21:24:30+11:00"
draft = false
title = "A few months with Neovim"

+++

I've been using [Neovim] on my laptop for the last few months. I don't do
a lot of editing at home these days, but I have been using it occasionally on
hobby projects and to edit this site's content and structure.

## First impressions

First impressions were a little bit rough. Neovim refused to recognise my
`.vimrc`. It turns out they've changed where this is kept to the more standard
`.config/nvim` directory. Whilst as a developer I understand why this is a good
thing, as a user I'd have loved a migration period or at the very least a
deprecation notice. At least this was all easily solved with:

```bash
ln -s $HOME/.vim $HOME/.config/nvim
ln -s $HOME/.vimrc $HOME/.config/nvim/init.vim
```

I also ran into a couple of other issues:

 - Interaction with the system clipboard is missing out of the box. You need to
   install a 'clipboard provider' (something like `xclip` or `xsel`).
 - Spellcheck also doesn't work right away. You'll need to wait a few minutes
   for it to download language files after you initially `:set spell`.
 - Plugins require you to `pip install` a couple of packages first due to the
   new interface for writing plugins.

Aside from these quibbles, everything seem to work well. All of my plugins still
do the right thing and I can edit just fine.

## Is it worth it?

Not yet. Honestly my experience with Neovim has thus far been at best exactly
the same as vanilla vim and at worst extremely frustrating (try to actually find
documentation on the `.vimrc` location change). As yet, Neovim doesn't seem to
offer any added value over vim and it has many drawbacks.

I was very excited by this project initially, so I can only hope that they can
pull it together and demonstrate why the average user would care about or use
their fork. Until then I'll try to keep on using it, but I simply can't
recommend it to others.


[Neovim]: https://neovim.io/
