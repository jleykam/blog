+++
date = 2014-06-09T05:03:41Z
draft = false
title = "Hello, World!"
+++

Hello! Welcome to my terrible blog! Terrible because I write for me and not for
you, so most of my posts are just rambling half-thoughts. Perhaps you'll find
something worth reading all the same.

## About Me

I'm a 22 year old software engineer living in Sydney, Australia. I sleep a lot,
play far too many games, read not nearly enough books, sometimes go on motorbike
adventures and am a keen picnic-er. I also have ill-founded opinions about
different topics in programming!

I have a Bachelor of Computer Science and Technology from the University of
Sydney and am currently working on the Places API at Google. Specifically, I'm
pretending to be an Android developer working on a lot of the nitty gritty bits
of our Android API.

My configuration files for all the stuff I use (mostly vim and zsh) can be found
on [Github]. [This blog] now uses the latest in cutting edge hipster
technologies. Posts are written in markdown and assembled into a site from
templates using [Hugo]. The styling is done with [Sass] and [Grunt] is used to
automate most of the process.  I also use [Google Webfonts] and [Fontello] as I
have opinions about typography.

You should also check out [Daniel's blog]. It's of a similar vein to mine, only
he's more adorable.

[Github]: https://www.github.com/jqln-0/dotfiles
[This blog]: https://bitbucket.org/jqln/blog
[Sass]: http://sass-lang.com/
[Hugo]: http://hugo.spf13.com/
[Grunt]: http://gruntjs.com/
[Google Webfonts]: https://www.google.com/fonts
[Fontello]: http://fontello.com/
[Daniel's blog]: http://dbyron.id.au
