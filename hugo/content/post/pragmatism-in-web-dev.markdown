+++
Tags = ["webdev"]
date = "2014-10-11T22:58:26+11:00"
draft = false
title = "Pragmatism in Web Development"

+++

As a part of my final semester of university, I've been working on a web
application designed to help educate people in cultural competence with regards
to Aboriginal Australians. The application is based around the idea of
geographic 'songlines' and how they relate to various sacred sites and
landmarks. The actual content of the application isn't really that important,
however I've found it to be an extremely interesting experience in terms of the
technologies used and the general approach to web development taken.

My reference point for web development coming into the project was essentially
just this website (I also created a
[LightDM Webkit theme](https://github.com/jqln-0/Bevel) in the past, but I don't
really count it as a 'web' thing). I like to think that this site is pretty
good, technically speaking. It's all valid HTML5, it responds properly to mobile
devices and it loads super quick because I've put a bit of time into making sure
all of my resources are small and don't rely on too many third party components.
I even feel a little bad for using Pure CSS because it seems a bit wasteful!

The songlines webapp ('Virtual Journey') is not like this. Virtual Journey is a
Ruby on Rails app that uses 95 different bundles (and counting!) and a handful
of huge CSS frameworks, including Bootstrap *and* jQuery UI. There's a huge
asset pipeline that basically concatenates all of our resources together (so
every page loads every line of javascript, no matter how irrelevant) and even if
the result of this wasn't huge, every page has an absolutely absurd number of
linked stylesheets and scripts. The front page has a 4.7MiB JPEG on the front of
it. Obviously there's a lot of different factors leading to these differences.
Virtual Journey is much more sophisticated than my little static site. It
requires a database and secure logins and such. We're also quite pressed for
time. Working with multiple developers is also a factor, as we all have
different skill levels and degrees of understanding of the technologies we're
using.

I guess my point in all this is that it's really interesting to look at the
differences between my own personal webpage, which I can spend as much time as I
like tinkering with, and a 'real' web application being created on a fixed
timeline. Whilst it would be nice to have all the time in the world to ensure
all of our pages are perfectly valid and tailor our external dependencies to our
needs, the need to be pragmatic and just get things working as quickly as
possibly wins out. I can only hope that in my next foray into web development I
will be able to strike a balance between the two.
