#!/bin/bash

set -e
rm -rf build
grunt dist
rsync -avz --delete --progress build/ ssh.jacqueline.id.au:/var/www/jacqueline.id.au/
