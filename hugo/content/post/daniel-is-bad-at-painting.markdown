+++
Tags = ["miniatures"]
date = "2016-01-02T13:37:36+11:00"
draft = false
title = "Daniel is bad at painting"
+++

Or more accurately, he's bad at following through. It was his idea to pick up
paints, but he's not finished a single mini! Here is what he's managed to do so
far:

{{< img src="IMG_20160102_134551.jpg" title="Daniel's minis" >}}

Full disclosure: Daniel hasn't actually touched the E-Web engineer at all since
I primed it for him. Here's what I've done:

{{< img src="IMG_20160102_134643.jpg" title="Behold my pretty minis" >}}

What a slacker! More fun pictures:

{{< img src="IMG_20160102_134731.jpg" title="Finished Stormtroopers" >}}

{{< img src="IMG_20160102_134814.jpg" title="Probe Droids" >}}

{{< img src="IMG_20160102_134848.jpg" title="E-Web Engineer" >}}
