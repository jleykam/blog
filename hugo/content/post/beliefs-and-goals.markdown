+++
Tags = ["rpg"]
date = 2014-07-07T06:35:54Z
draft = false
title = "Beliefs and Goals"

+++

I recently managed to get my hands on a copy of Mouse Guard RPG. It's an out of
print variant of the Burning Wheel system set in David Petersen's brilliant
Mouse Guard universe. The system includes a whole bunch of really interesting
ideas and mechanics which I hope to integrate into how I run my games. In
particular, I'm interested in the 'beliefs and goals' system used.

This system is a way of defining characters in terms of how they tend to view
the world (their 'beliefs') as well as their immediate goals.

## Beliefs

A character's belief is, as previously stated, a brief summary of how they view
the world. Examples given in Mouse Guard are things like "Saxon believes that
any problem can be solved with the point of his sword", or "A guardmouse needs
to be able to think with their head and act with their heart". These beliefs
colour every situation the character finds themself in, for example Saxon would
be the sort of person to take the first swing in a fight or attempt to coerce
someone through intimidation.

Whilst at face value being a very simple idea, incorporating beliefs into a
character can greatly improve the player's sense of their personality. Detailed
backstories and such can give context to what a character does, but having a
simple, once-sentence belief written down can be immensely helpful in quickly
deciding how a character will react to any given situation.

## Goals

Goals represent a character's short-term missions or desires. Ideally they
should be achievable within the span of one or two sessions and should be fairly
specific. "I will convince the patrol leader than I'm worthy of a promotion", or
"I must discover why my friend has stopped sending letters" are example goals.
Note the language used; "I must" and "I will". A goal should be a strong call to
action.

Goals are helpful in giving a character something to do beyond what the party as
a whole is doing, which helps to keep players immersed and interested in what's
going on.

## Summary

Mouse Guard's system of beliefs and goals is extremely helpful is defining a
character's personality and then playing to that personality in during a
session. Even if these ideas are not mechanically supported in other systems I
run, I will definitely be encouraging my players to make use of these ideas.

