+++
Tags = ["programming"]
date = "2015-10-21T21:25:39+11:00"
draft = false
title = "You should try zsh!"

+++

I've been using [zsh] consistently for a few years now. I'm not a power user by
any stretch of the imagination - I still don't really know how to configure it
properly - but it's massively increased my productivity and made using the shell
to complete tasks a lot nicer. What's so great about it?

### Awesome globbing

You can do this:

```sh
$ mv */*hdpi*
# Hit tab...
$ mv search/drawable-hdpi-search.png save/drawable-hdpi-save.png # etc...
```

This is just the tip of the iceberg. For example, you can also use `**` to glob
recursively.

### Flexible prompts

Here's what mine looks like:

{{< img-noscale src="my-zsh-prompt.png" title="My shell prompt">}}

It includes the current directory, my hostname, the git branch I'm on and
whether it's clean or dirty. All the information on the right disappears when
I'm typing a really long command. The prompt is based heavily on one of the ones
included in [oh my zsh] but modifying it to suit me needs was super simple.

### Easy shared history!

I've occasionally heard people talk about how they 'lost' a useful command due
to having multiple shells open. Zsh makes it really easy to have a command
history that's shared between shells and never overwritten. Keep every command
you've ever run!

### Much more 'fluid' tab completion

This overlaps a bit with globbing and is hard to explain properly. Zsh has all
sorts of little things like command-specific completions (bash has this too,
granted), spelling corrections and the globbing described above. Together
they're far more useful than individually and allow me type exactly the command
I want crazy fast.

There're lots more magical examples on the [zsh-lovers] page, but to be honest
most of them are outside anything I do in my shell on a regular basis. For me
the real utility is in all the little improvements I use every day.

### How do I try it out?

I'd recommend installing zsh in your OS's usual way, then adding one of the many
pre-configured settings distributions. I use [oh my zsh]! (Although mind their
install instructions make me angry. Yep, just pipe curl into sh, why not?)

[zsh]: http://zsh.sourceforge.net/
[oh my zsh]: https://github.com/robbyrussell/oh-my-zsh
[zsh-lovers]: http://grml.org/zsh/zsh-lovers.html
